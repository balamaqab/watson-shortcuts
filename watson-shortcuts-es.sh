#!/usr/bin/env bash

###########################################################
#
# Shortcuts for Watson (Spanish)
# http://tailordev.github.io/Watson/
#
###########################################################

function daybefore() {
    currentDay=$(LC_TIME=C date +%A)
    if [ $currentDay = "Monday" ]; then
        echo $(date -d 'last friday' -I)
    else
        echo $(date -d 'yesterday' -I)
    fi
}

function wday() {
    echo "Actividades $1"
    watson log --from $1 --to $1 --no-pager
}

function wdetail() {
    echo "SEMANA de $1 a $2"
    watson log --from $1 --to $2 --no-pager
}

function wcustom() {
    echo "SEMANA de $1 a $2"
    watson aggregate --from $1 --to $2 --no-pager
}

alias ahora="watson status"
alias fin="watson stop"
alias reinicia="watson restart"
alias edita="watson edit"
alias hoy="wday $(date -I)"
alias ayer="wday $(daybefore)"
alias diaanterior="wday $(daybefore)"
alias misemana="wdetail $(date -d 'last monday' -I) $(date -d 'today' -I)"
alias resumensemana="wcustom $(date -d 'last monday' -I) $(date -d 'today' -I)"
