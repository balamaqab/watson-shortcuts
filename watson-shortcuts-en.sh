#!/usr/bin/env bash

###########################################################
#
# Shortcuts for Watson (English)
# http://tailordev.github.io/Watson/
#
###########################################################

function daybefore() {
    currentDay=$(LC_TIME=C date +%A)
    if [ $currentDay = "Monday" ]; then
        echo $(date -d 'last friday' -I)
    else
        echo $(date -d 'yesterday' -I)
    fi
}

function wday() {
    echo "Activities $1"
    watson log --from $1 --to $1 --no-pager
}

function wdetail() {
    echo "WEEK from $1 to $2"
    watson log --from $1 --to $2 --no-pager
}

function wcustom() {
    echo "WEEK from $1 to $2"
    watson aggregate --from $1 --to $2 --no-pager
}

alias current="watson status"
alias end="watson stop"
alias stop="watson stop"
alias restart="watson restart"
alias fix="watson edit"
alias today="wday $(date -I)"
alias yesterday="wday $(daybefore)"
alias pastday="wday $(daybefore)"
alias myweek="wdetail $(date -d 'last monday' -I) $(date -d 'today' -I)"
alias weekoverview="wcustom $(date -d 'last monday' -I) $(date -d 'today' -I)"
